// const MongoClient = require("mongodb");
// const MongoClient = require("mongodb").MongoClient;

const { MongoClient, ObjectID } = require("mongodb");

const uri = "mongodb://127.0.0.1:27017";
const dbName = "coba1";

//https://docs.mongodb.com/drivers/node/current/quick-start/
const client = new MongoClient(uri, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

client.connect((error, client) => {
  if (error) {
    return console.log("Koneksi gagal");
  }
  console.log("Koneksi berhasil");
});

//pilih database
const db = client.db(dbName);

//menambahkan 1 data ke collection mahasiswa
db.collection("mahasiswa").insertOne(
  {
    nama: "Kenzo",
    email: "kenzo@gmail.com",
  },
  (error, result) => {
    if (error) {
      return console.log("gagal menambahkan data");
    }
    console.log(result);
  }
);

//menambahkan lebih dari 1 data
db.collection("mahasiswa").insertMany(
  [
    {
      nama: "Kenken",
      email: "kenken@gmail.com",
    },
    {
      nama: "Tintin",
      email: "tintin@gmail.com",
    },
  ],
  (error, result) => {
    if (error) {
      return console.log("data gagal ditambahkan");
    }
    console.log(result);
  }
);

//menampilkan semua data yang ada di collection "mahasiswa"
console.log(
  db
    .collection("mahasiswa")
    .find()
    .toArray((error, result) => {
      console.log(result);
    })
);

//menampilkan data berdasarkan kriteria
console.log(
  db
    .collection("mahasiswa")
    // .find({ nama: "Kenken" })
    .find({ _id: ObjectID("60e141e825f39f5024b88a97") })
    .toArray((error, result) => {
      console.log(result);
    })
);

//mengubah data based on ID
// https://docs.mongodb.com/manual/reference/method/db.collection.updateOne/
const updatePromise = db.collection("mahasiswa").updateOne(
  {
    _id: ObjectID("60e141e825f39f5024b88a97"),
  },
  {
    $set: {
      email: "antontin@gmail.com",
    },
  }
);

updatePromise
  .then((result) => {
    console.log(result);
  })
  .catch((error) => {
    console.log(error);
  });

//mengubah data lebih dari 1 berdasarkan kriteria
db.collection("mahasiswa").updateMany(
  {
    nama: "Valin",
  },
  {
    $set: {
      nama: "Valin Edit",
    },
  }
);

//menghapus 1 data
db.collection("mahasiswa")
  .deleteOne({
    _id: ObjectID("60e141e825f39f5024b88a96"),
  })
  .then((result) => {
    console.log(result);
  })
  .catch((error) => {
    console.log(error);
  });

//menghapus lebih dari 1 data
db.collection("mahasiswa")
  .deleteMany({
    nama: "Tiga",
  })
  .then((result) => {
    console.log(result);
  })
  .catch((error) => {
    console.log(error);
  });
